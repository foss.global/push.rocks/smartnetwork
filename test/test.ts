import { expect, expectAsync, tap } from '@pushrocks/tapbundle';
import * as smartnetwork from '../ts/index.js';

let testSmartNetwork: smartnetwork.SmartNetwork;

tap.test('should create a valid instance of SmartNetwork', async () => {
  testSmartNetwork = new smartnetwork.SmartNetwork();
  expect(testSmartNetwork).toBeInstanceOf(smartnetwork.SmartNetwork);
});

tap.test('should perform a speedtest', async () => {
  const result = await testSmartNetwork.getSpeed();
  console.log(`Download speed for this instance is ${result.downloadSpeed}`);
  console.log(`Upload speed for this instance is ${result.uploadSpeed}`);
});

tap.test('should determine wether a port is free', async () => {
  await expectAsync(testSmartNetwork.isLocalPortUnused(8080)).toBeTrue();
});

tap.test('should scan a port', async () => {
  await expectAsync(testSmartNetwork.isRemotePortAvailable('lossless.com:443')).toBeTrue();
  await expectAsync(testSmartNetwork.isRemotePortAvailable('lossless.com', 443)).toBeTrue();
  await expectAsync(testSmartNetwork.isRemotePortAvailable('lossless.com:444')).toBeFalse();
});

tap.test('should get gateways', async () => {
  const gatewayResult = await testSmartNetwork.getGateways();
  console.log(gatewayResult);
});

tap.test('should get the default gateway', async () => {
  const gatewayResult = await testSmartNetwork.getDefaultGateway();
  console.log(gatewayResult);
});

tap.test('should get public ips', async () => {
  const ips = await testSmartNetwork.getPublicIps();
  console.log(ips);
});

tap.start();
